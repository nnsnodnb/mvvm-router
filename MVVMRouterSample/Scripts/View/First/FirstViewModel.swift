//
//  FirstViewModel.swift
//  MVVMRouterSample
//
//  Created by Oka Yuya on 2019/12/06.
//  Copyright © 2019 Yuya Oka. All rights reserved.
//

import RxCocoa
import RxSwift

protocol FirstViewModelInput {

    var nextTrigger: PublishRelay<Void> { get }
}

protocol FirstViewModelOutput {}

protocol FirstViewModel {

    var input: FirstViewModelInput { get }
    var output: FirstViewModelOutput { get }
    var router: FirstRouter { get }
    init(router: FirstRouter)
}

final class FirstViewModelImpl: FirstViewModel {

    var input: FirstViewModelInput { return self }
    var output: FirstViewModelOutput { return self }

    let nextTrigger: PublishRelay<Void> = .init()
    let dismissCompletion: PublishRelay<Void> = .init()
    let router: FirstRouter

    private let disposeBag = DisposeBag()

    init(router: FirstRouter) {
        self.router = router

        nextTrigger
            .subscribe(onNext: {
                router.transitionToSecond()
            })
            .disposed(by: disposeBag)
    }
}

// MARK: - FirstViewModelInput

extension FirstViewModelImpl: FirstViewModelInput {}

// MARK: - FirstViewModelOutput

extension FirstViewModelImpl: FirstViewModelOutput {}
