//
//  FirstRouter.swift
//  MVVMRouterSample
//
//  Created by Oka Yuya on 2019/12/06.
//  Copyright © 2019 Yuya Oka. All rights reserved.
//

import RxCocoa
import RxSwift
import UIKit

protocol FirstRouter {

    init(view: FirstViewProtocol)
    func transitionToSecond()
}

final class FirstRouterImpl: FirstRouter {

    private(set) weak var view: FirstViewProtocol!

    private let disposeBag = DisposeBag()

    init(view: FirstViewProtocol) {
        self.view = view
    }

    func transitionToSecond() {
        let viewController = SecondViewController()
        let secondRouter = SecondRouterImpl(view: viewController)
        let viewModel = SecondViewModelImpl(router: secondRouter)
        viewController.inject(.init(viewModel: viewModel))

        view.present(viewController, animated: true, completion: nil)
    }
}
