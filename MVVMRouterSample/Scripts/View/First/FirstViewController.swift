//
//  FirstViewController.swift
//  MVVMRouterSample
//
//  Created by Oka Yuya on 2019/12/06.
//  Copyright © 2019 Yuya Oka. All rights reserved.
//

import RxCocoa
import RxSwift
import UIKit

protocol FirstViewProtocol: UIViewController {}

final class FirstViewController: UIViewController {

    @IBOutlet private weak var nextButton: UIButton! {
        didSet { nextButton.rx.tap.bind(to: viewModel.input.nextTrigger).disposed(by: disposeBag) }
    }

    private let disposeBag = DisposeBag()

    private var viewModel: FirstViewModel!
}

extension FirstViewController {

    struct Dependency {
        let viewModel: FirstViewModel
    }

    func inject(_ dependency: Dependency) {
        viewModel = dependency.viewModel
    }
}

// MARK: - FirstViewProtocol

extension FirstViewController: FirstViewProtocol {}
