//
//  SecondViewModel.swift
//  MVVMRouterSample
//
//  Created by Oka Yuya on 2019/12/06.
//  Copyright © 2019 Yuya Oka. All rights reserved.
//

import RxCocoa
import RxSwift

protocol SecondViewModelInput {

    var closeTrigger: PublishRelay<Void> { get }
}

protocol SecondViewModelOutput {}

protocol SecondViewModel {

    var input: SecondViewModelInput { get }
    var output: SecondViewModelOutput { get }
    var router: SecondRouter { get }
    init(router: SecondRouter)
}

final class SecondViewModelImpl: SecondViewModel {

    let router: SecondRouter
    let closeTrigger: PublishRelay<Void> = .init()

    var input: SecondViewModelInput { return self }
    var output: SecondViewModelOutput { return self }

    private let disposeBag = DisposeBag()

    init(router: SecondRouter) {
        self.router = router

        closeTrigger
            .subscribe(onNext: { [router] in
                router.dismiss()
            })
            .disposed(by: disposeBag)
    }
}

// MARK: - SecondViewModelInput

extension SecondViewModelImpl: SecondViewModelInput {}

// MARK: - SecondViewModelOutput

extension SecondViewModelImpl: SecondViewModelOutput {}
