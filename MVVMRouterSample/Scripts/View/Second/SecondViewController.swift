//
//  SecondViewController.swift
//  MVVMRouterSample
//
//  Created by Oka Yuya on 2019/12/06.
//  Copyright © 2019 Yuya Oka. All rights reserved.
//

import RxCocoa
import RxSwift
import UIKit

protocol SecondViewProtocol: UIViewController {}

final class SecondViewController: UIViewController {

    @IBOutlet private weak var closeButton: UIButton! {
        didSet { closeButton.rx.tap.bind(to: viewModel.input.closeTrigger).disposed(by: disposeBag) }
    }

    private let disposeBag = DisposeBag()

    private var viewModel: SecondViewModel!
}

extension SecondViewController {

    struct Dependency {
        let viewModel: SecondViewModel
    }

    func inject(_ dependency: Dependency) {
        viewModel = dependency.viewModel
    }
}

// MARK: - SecondViewProtocol

extension SecondViewController: SecondViewProtocol {}
