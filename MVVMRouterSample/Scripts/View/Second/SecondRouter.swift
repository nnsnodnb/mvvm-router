//
//  SecondRouter.swift
//  MVVMRouterSample
//
//  Created by Oka Yuya on 2019/12/06.
//  Copyright © 2019 Yuya Oka. All rights reserved.
//

import UIKit

protocol SecondRouter {

    init(view: SecondViewProtocol)
    func dismiss()
}

final class SecondRouterImpl: SecondRouter {

    private(set) weak var view: SecondViewProtocol!

    init(view: SecondViewProtocol) {
        self.view = view
    }

    func dismiss() {
        view.dismiss(animated: true, completion: nil)
    }
}
