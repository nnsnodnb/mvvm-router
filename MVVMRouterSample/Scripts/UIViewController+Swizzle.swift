//
//  UIViewController+Swizzle.swift
//  MVVMRouterSample
//
//  Created by Oka Yuya on 2019/12/06.
//  Copyright © 2019 Yuya Oka. All rights reserved.
//

import DeallocationChecker
import UIKit

extension UIViewController {

    class func swizzleViewDidDisappear() {
        let fromMethod = class_getInstanceMethod(self, #selector(viewDidDisappear(_:)))!
        let toMethod = class_getInstanceMethod(self, #selector(overrideViewDidDisappear(_:)))!
        method_exchangeImplementations(fromMethod, toMethod)
    }

    @objc private func overrideViewDidDisappear(_ animated: Bool) {
        overrideViewDidDisappear(animated)
        #if DEBUG
        DeallocationChecker.shared.checkDeallocation(of: self)
        #endif
    }
}
